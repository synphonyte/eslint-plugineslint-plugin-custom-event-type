/**
 * @fileoverview Ensures that CustomEvent types are all named in kebab-case.
 * @author Marc-Stefan Cassola
 */
'use strict';

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require( '../../../lib/rules/kebab-case' ),
  RuleTester = require( 'eslint' ).RuleTester;


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

const getErrors = ( suggestedOutput ) => [
  {
    message: 'Only use kebab-case in CustomEvent types',
    type: 'Literal',
    suggestions: [ {
      desc: 'Convert type to kebab-case',
      output: suggestedOutput,
    } ],
  },
];

const ruleTester = new RuleTester();
ruleTester.run( 'kebab-case', rule, {
  valid: [
    { code: 'new CustomEvent(\'camel-case\')' },
    { code: 'new CustomEvent(\'camel9-9case\')' },
    { code: 'new CustomEvent(\'single\')' },
  ],

  invalid: [
    {
      code: 'new CustomEvent(\'camelCaseEvent\')',
      errors: getErrors('new CustomEvent(\'camel-case-event\')'),
    },
    {
      code: 'new CustomEvent(\'Uppercase1-event\')',
      errors: getErrors('new CustomEvent(\'uppercase1-event\')'),
    },
    {
      code: 'new CustomEvent(\'Event with spaces\')',
      errors: getErrors('new CustomEvent(\'event-with-spaces\')'),
    },
  ],
} );
