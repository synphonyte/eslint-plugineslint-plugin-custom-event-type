# eslint-plugin-custom-event-type

Ensures the consistent spelling of CustomEvent types. For example all camelCase or all kebab-case.

## Installation

You'll first need to install [ESLint](https://eslint.org/):

```sh
npm i eslint --save-dev
```

Next, install `eslint-plugin-custom-event-type`:

```sh
npm install eslint-plugin-custom-event-type --save-dev
```

## Usage

Add `custom-event-type` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "custom-event-type"
    ]
}
```


Then configure the rules you want to use under the rules section.

```json
{
    "rules": {
      "custom-event-type/kebab-case": [ "error" ]
    }
}
```

## Supported Rules

* Fill in provided rules here


