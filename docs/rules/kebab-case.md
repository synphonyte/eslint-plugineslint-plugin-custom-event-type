# Ensures that CustomEvent types are all named in kebab-case. (kebab-case)

This rule aims to ensure kebab-case spelling of CustomEvent types.

Examples of **incorrect** code for this rule:

```js
new CustomEvent( 'camelCaseEvent' );
new CustomEvent( 'Uppercase1-event' );
new CustomEvent( 'Event with spaces' );
```

Examples of **correct** code for this rule:

```js
new CustomEvent( 'camel-case' );
new CustomEvent( 'camel-case-with-9numbers1' );
new CustomEvent( 'single' );
```
