/**
 * @fileoverview Ensures the consistent spelling of CustomEvent types. For example all camelCase or all kebab-case.
 * @author Marc-Stefan Cassola
 */
"use strict";

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const requireIndex = require("requireindex");

//------------------------------------------------------------------------------
// Plugin Definition
//------------------------------------------------------------------------------


// import all rules in lib/rules
module.exports.rules = requireIndex(__dirname + "/rules");



