/**
 * @fileoverview Ensures that CustomEvent types are all named in kebab-case.
 * @author Marc-Stefan Cassola
 */
'use strict';

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

/**
 * @type {import('eslint').Rule.RuleModule}
 */
module.exports = {
  meta: {
    type: 'suggestion', // `problem`, `suggestion`, or `layout`
    docs: {
      description: 'Ensures that CustomEvent types are all named in kebab-case.',
      category: 'Suggestions',
      recommended: true,
      url: null, // URL to the documentation page for this rule
    },
    fixable: null, // Or 'code' or `whitespace`
    hasSuggestions: true,
    schema: [], // Add a schema if the rule has options
  },

  create( context ) {
    const kebabCasePattern = /^([a-z0-9])+(-[a-z0-9]+)*$/;

    //----------------------------------------------------------------------
    // Helpers
    //----------------------------------------------------------------------

    // any helper functions should go here or else delete this section

    //----------------------------------------------------------------------
    // Public
    //----------------------------------------------------------------------

    return {
      NewExpression( node ) {
        if ( node.callee.name === 'CustomEvent' && !kebabCasePattern.test( node.arguments[ 0 ].value ) ) {
          context.report( {
            node: node.arguments[ 0 ],
            message: 'Only use kebab-case in CustomEvent types',

            suggest: [
              {
                desc: 'Convert type to kebab-case',
                fix( fixer ) {
                  const kebabCaseType = node.arguments[ 0 ].value
                    .replace( /([a-z0-9])([A-Z])/g, '$1-$2' )
                    .replace( /[\s_]+/g, '-' )
                    .toLowerCase();

                  return [
                    fixer.replaceText( node.arguments[ 0 ], `'${ kebabCaseType }'` ),
                  ];
                },
              },
            ],
          } );
        }
      },
    };
  },
};
